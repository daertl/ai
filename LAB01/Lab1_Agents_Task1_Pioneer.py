# Make sure to have the server side running in V-REP:
# in a child script of a V-REP scene, add following command
# to be executed just once, at simulation start:
#
# simExtRemoteApiStart(19999)
# then start simulation, and run this program.
#
# IMPORTANT: for each successful call to simxStart, there
# should be a corresponding call to simxFinish at the end!
import Lab1_Agents_Task1_World as World
import random

memoryCollision = [5]
collisionCounter = 0
index = 0
# connect to the server
robot = World.init()
# print important parts of the robot
print(sorted(robot.keys()))


# random agent to drive randomly and collect energy blocks
def random_agent():
    simulationTime = World.getSimulationTime()

    motorSpeed = dict(speedLeft=1, speedRight=1)
    print("Trying to collect a block...", World.collectNearestBlock())
    World.execute(motorSpeed, simulationTime, 1000)
    randomNumRight = random.uniform(2, 5)
    motorSpeed = dict(speedLeft=0, speedRight=randomNumRight)
    print("Trying to collect a block...", World.collectNearestBlock())
    World.execute(motorSpeed, simulationTime, 200)
    motorSpeed = dict(speedLeft=1, speedRight=1)
    print("Trying to collect a block...", World.collectNearestBlock())
    World.execute(motorSpeed, simulationTime, 1000)
    randomNumLeft = random.uniform(2, 5)
    motorSpeed = dict(speedLeft=randomNumLeft, speedRight=0)
    print("Trying to collect a block...", World.collectNearestBlock())
    World.execute(motorSpeed, simulationTime, 200)
    print("Trying to collect a block...", World.collectNearestBlock())


# fixed agent drives with the same actions arount
def fixed_agent():

    if simulationTime < 15000:
        motorSpeed = dict(speedLeft=1, speedRight=1)
        print("Trying to collect a block...", World.collectNearestBlock())

    elif simulationTime < 18000:
        motorSpeed = dict(speedLeft=-1, speedRight=1)
        print("Trying to collect a block...", World.collectNearestBlock())

    elif simulationTime < 37000:
        motorSpeed = dict(speedLeft=1, speedRight=1)
        print("... got dizzy, stopping!")
        print("Trying to collect a block...", World.collectNearestBlock())

    elif simulationTime < 42000:
        motorSpeed = dict(speedLeft=1, speedRight=-1)

    else:
        motorSpeed = dict(speedLeft=0, speedRight=0)
        print("Starting of fixed agent")
        motorSpeed = dict(speedLeft=1, speedRight=1)
        World.execute(motorSpeed, simulationTime, 10000)
        print("Trying to collect a block...", World.collectNearestBlock())
        motorSpeed = dict(speedLeft=-1, speedRight=1)
        print("Trying to collect a block...", World.collectNearestBlock())
        World.execute(motorSpeed, simulationTime, 5)
        motorSpeed = dict(speedLeft=1, speedRight=1)
        print("Trying to collect a block...", World.collectNearestBlock())
        World.execute(motorSpeed, simulationTime, 10000)
        motorSpeed = dict(speedLeft=1, speedRight=-1)
        print("Trying to collect a block...", World.collectNearestBlock())
        World.execute(motorSpeed, simulationTime, 5)
        print("Trying to collect a block...", World.collectNearestBlock())

    World.setMotorSpeeds(motorSpeed)


# reflex agent detects walls and collects energy blocks
def reflex_agent():
    sensorLeft = float(str(World.getSensorReading("ultraSonicSensorLeft")))
    sensorRight = float(str(World.getSensorReading("ultraSonicSensorRight")))

    # Get the distance and direction of the next energy block
    energyBlockPos = World.getSensorReading("energySensor")
    energyDistance = energyBlockPos['distance']
    energyDirection = energyBlockPos['direction']
    motorSpeed = dict(speedLeft=1.5, speedRight=1.5)

    if(energyDistance < 2):
        World.collectNearestBlock()
        print("Block nearby: ", energyDistance)
        while(True):
            print("in while loop: ", energyDirection)

            if(energyDirection > 0):
                motorSpeed = dict(speedLeft=1, speedRight=-1)

            elif(energyDirection < 0):
                motorSpeed = dict(speedLeft=-1, speedRight=1)

            World.setMotorSpeeds(motorSpeed)
            energyBlockPosTemp = World.getSensorReading("energySensor")
            energyDirection = energyBlockPosTemp['direction']

            if(energyDirection < -0.07 or energyDirection > 0.07):
                break

    if(sensorLeft < 0.3):
        motorSpeed = dict(speedLeft=3, speedRight=-3.2)
        World.setMotorSpeeds(motorSpeed)
        print("LeftSensor ", sensorLeft)
        print("Collision detected")

    elif(sensorRight < 0.35):
        motorSpeed = dict(speedLeft=-3.2, speedRight=3)
        World.setMotorSpeeds(motorSpeed)
        print("RightSensor ", sensorRight)

    elif(sensorLeft < 0.5 or sensorRight < 0.5):
        motorSpeed = dict(speedLeft=1, speedRight=1)
        World.setMotorSpeeds(motorSpeed)

    elif(sensorLeft < 0.4 or sensorRight < 0.4):
        motorSpeed = dict(speedLeft=0.5, speedRight=0.5)
        World.setMotorSpeeds(motorSpeed)

    else:
        motorSpeed = dict(speedLeft=1.5, speedRight=1.5)
        World.collectNearestBlock()
        World.setMotorSpeeds(motorSpeed)


# memory agent drives around and memorize walls
def memory_agent():

    sensorLeft = float(str(World.getSensorReading("ultraSonicSensorLeft")))
    sensorRight = float(str(World.getSensorReading("ultraSonicSensorRight")))

    # add 1 for left and 2 for right collision
    global memoryCollision
    global index
    global collisionCounter

    energyBlockPos = World.getSensorReading("energySensor")
    energyDistance = energyBlockPos['distance']
    energyDirection = energyBlockPos['direction']

    # Detect and turn to a Energy block
    if(energyDistance < 2):
        World.collectNearestBlock()
        # print("Block nearby: ", energyDistance)
        while(True):

            if(energyDirection > 0):
                motorSpeed = dict(speedLeft=1, speedRight=-1)

            elif(energyDirection < 0):
                motorSpeed = dict(speedLeft=-1, speedRight=1)
            World.setMotorSpeeds(motorSpeed)
            energyBlockPosTemp = World.getSensorReading("energySensor")
            energyDirection = energyBlockPosTemp['direction']

            if(energyDirection < -0.07 or energyDirection > 0.07):
                break

    if(sensorLeft < 0.3):
        motorSpeed = dict(speedLeft=3, speedRight=-3.2)
        memoryCollision.append(1)
        index += 1

        if(memoryCollision[index-1] == 2):
            collisionCounter = 0

        countUpCollision()
        print("Collision detected")

    elif(sensorRight < 0.35):
        motorSpeed = dict(speedLeft=-3.2, speedRight=3)
        memoryCollision.append(2)
        index += 1

        if(memoryCollision[index-1] == 1):
            collisionCounter = 0

        countUpCollision()

    elif(sensorLeft < 0.5 or sensorRight < 0.5):
        motorSpeed = dict(speedLeft=1, speedRight=1)

    elif(sensorLeft < 0.4 or sensorRight < 0.4):
        motorSpeed = dict(speedLeft=0.5, speedRight=0.5)

    elif(collisionCounter > 4):
        motorSpeed = dict(speedLeft=-3, speedRight=3)
        collisionCounter = 0
        print("memory reaction")

    else:
        motorSpeed = dict(speedLeft=1.5, speedRight=1.5)
        World.collectNearestBlock()

    World.setMotorSpeeds(motorSpeed)


def countUpCollision():
    global collisionCounter
    if(collisionCounter is None):
        collisionCounter = 0
    collisionCounter = collisionCounter + 1


# main Control loop
while robot:
    #######################################################
    # Perception Phase: Get information about environment #
    #######################################################

    simulationTime = World.getSimulationTime()

    # random_agent()
    # fixed_agent()
    # reflex_agent()
    memory_agent()

