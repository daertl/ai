import random


# Rank dicionary to calculate the points
rankPoints = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, 'T': 10, 'J': 11, 'Q': 12, 'K': 13, 'A': 14 }

# points which are been added
pair = 13
threeOfCard = 26

# Kreuz: Clubs, Pik: Spades, Herz: Hearts, Karo: Diamonds, T: Ten
availableCards = ['Ah', 'Ad', 'Ac', 'As', 'Kh', 'Kd', 'Kc', 'Ks', 'Qh', 'Qd', 'Qc', 'Qs', 'Jh', 'Jd', 'Jc', 'Js', 'Th', 'Td', 'Tc', 'Ts', '9h', '9d', '9c', '9s', '8h', '8d', '8c', '8s', '7h', '7d', '7c', '7s', '6h', '6d', '6c', '6s', '5h', '5d', '5c', '5s', '4h', '4d', '4c', '4s', '3h', '3d', '3c', '3s', '2h', '2d', '2c', '2s']

# amount of total cards
cardAmount = 52

# 2 example poker hands
currentHand1 = []
currentHand2 = []

# bidding Agent one
biddingAgent1 = []
biddingAgent2 = []
moneyPot = 0

# Points
agentOnePoints = 0
agentTwoPoints = 0

# Money
agentOneMoney = 0
agentTwoMoney = 0


# Randomly generate two hands of n cards
def generate2Hands(nn_card=3):
    usedCards = []
    index = 0
    for i in range(0, nn_card+nn_card):
        randomCard = int(random.uniform(0, cardAmount-1))
        index = index + 1
        for i in usedCards:
            # print("in for 1")
            if (randomCard == i):
                # print("card already used")
                randomCard = int(random.uniform(0, cardAmount-1))
            else:
                # print("continue")
                pass

        if (index % 2 == 0):
            currentHand2.append(availableCards[randomCard])
            usedCards.append(randomCard)
        else:
            currentHand1.append(availableCards[randomCard])
            usedCards.append(randomCard)

        # print("randomNum: ", usedCards)

    print("Deck1: ", currentHand1)
    print("Deck2: ", currentHand2)


# random function for bidding
def randomBidding(biddingAgent):
    global moneyPot

    for i in range(0, 3):
        biddingAgent.append(int(random.uniform(0, 50)))
        moneyPot += biddingAgent[i]
    pass


# fixed agent for bidding
def fixedBidding(biddingAgent):
    global moneyPot

    for i in range(0, 3):
        biddingAgent.append(25)
        moneyPot += biddingAgent[i]
    pass


# identify hand category using IF-THEN rule
def identifyHand(hand):
    foundThree = False
    agentPoints = 0
    for c1 in hand:
        for c2 in hand:  # only output three of a kind when found
            if (c1[0] == c2[0]) and (c1[1] < c2[1]) and foundThree is not True:
                agentPoints = rankPoints[c1[0]]+pair
                # print("Points: ", agentPoints)
            for c3 in hand:
                if (c1[0] == c2[0]) and (c2[0] == c3[0]) and (c1[1] < c2[1]) and (c2[1] < c3[1]):
                    agentPoints = rankPoints[c1[0]]+threeOfCard
                    foundThree = True
                elif (rankPoints[c1[0]] > rankPoints[c2[0]]) and (rankPoints[c2[0]] > rankPoints[c3[0]]):
                    agentPoints = rankPoints[c1[0]]

    return agentPoints


# identfy winner function
def identifyWinner():
    global agentOneMoney
    global agentTwoMoney
    global moneyPot
    if(agentOnePoints is None or agentTwoPoints is None):
        print("something went wrong!")
        pass

    if(agentOnePoints > agentTwoPoints):
        print("The winner is Agent one!")
        agentOneMoney += moneyPot
        moneyPot = 0
    elif(agentOnePoints < agentTwoPoints):
        print("The winner is Agent two!")
        agentTwoMoney += moneyPot
        moneyPot = 0
    else:
        print("No one won money pot stayed filled")


# calculate funciton for the difference
def calculateDifference(agentOneMoney, agentTwoMoney):
    if (agentOneMoney > agentTwoMoney):
        difference = agentOneMoney - agentTwoMoney
        print("The difference is: ", difference)
    else:
        difference = agentTwoMoney - agentOneMoney
        print("The difference is: ", difference)

# reflexed agent for bidding
def reflexedBidding(bidding, hand):
    global moneyPot
    points = identifyHand(hand)
    for i in range(0, 3):
        if (points <= 14):
            bidding.append(points - 2)
            moneyPot += bidding[i]
        elif (points >= 15) and (points < 27):
            bidding.append(points + 5)
            moneyPot += bidding[i]
        else:
            bidding.append(points + 10)
            moneyPot += bidding[i]


#########################
#      Game flow        #
#########################
for i in range(0, 50):

    #########################
    # phase 1: Card Dealing #
    #########################
    generate2Hands()

    #########################
    # phase 2:   Bidding    #
    #########################
    randomBidding(biddingAgent1)
    # fixedBidding(biddingAgent2)
    reflexedBidding(biddingAgent2, currentHand2)
    # randomBidding(biddingAgent2)
    print("Bidding agent 1: ", biddingAgent1)
    print("Bidding agent 2: ", biddingAgent2)
    print("Money pot: ", moneyPot)
    agentOnePoints = identifyHand(currentHand1)
    # agentOnePoints = analyseHand(currentHand1)
    agentTwoPoints = identifyHand(currentHand2)
    # agentTwoPoints = analyseHand(currentHand2)

    #########################
    # phase 2:   Showdown   #
    #########################
    print("Agent 1: ", agentOnePoints)
    print("Agent 2: ", agentTwoPoints)
    identifyWinner()
    print("--------------------------")
    # Clear the used variables
    currentHand1 = []
    currentHand2 = []
    biddingAgent1 = []
    biddingAgent2 = []

# calculate the difference between the amount of money
print("End result Agent1:", agentOneMoney)
print("End result Agent2:", agentTwoMoney)
calculateDifference(agentOneMoney, agentTwoMoney)


